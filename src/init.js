import Controller from './lib/controller'
import config from './config'

const uri = config.env["dev"].apiAdmin
console.log(config.env["dev"].apiAdmin)
global.controller = new Controller(uri)

export default global.controller