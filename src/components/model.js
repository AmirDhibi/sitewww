import controller from '../init'
export default new class {
    constructor () {
        this.listeP = []
        this.totalLength = []
        this.listMedia = []
        this.portfolioJSON = []
    }

  
    getMedia = () => {
    controller.api(['getMedia', {
        filter: {}
      }])
        .then(j => {
          this.listMedia = j.data.body.value.map(v => v.ID)
          console.log(this.listMedia)
        })
        return this.listMedia
    }
    getList = () => {
        controller.api(['getProduct', {
          filter: {}
        }])
          .then(j => {
            this.listeP = j.data.body.value
            console.log(this.listeP[0].marketing.uriModel3D[0])
            this.listeP.forEach(v => {
              v.stock = null
              v.dateNextProduction = null
              v.venteMensuelle = null
            })
            this.totalLength = j.data.body.total
          })
          return this.listeP
      }
   
  

}  