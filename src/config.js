export default {
    env: {
      dev: {
        admin: 'http://127.0.0.1:8080/',
        site: 'https://devadmin.3dmap.fr/',
        apiAdmin: 'https://devapi.3dmap.fr:8001'
      },
      tst: {
        admin: 'https://testadmin.3dmap.fr/',
        site: 'https://test.3dmap.fr/',
        apiAdmin: 'https://testapi.3dmap.fr:8001'
      },
      prod: {
        admin: 'https://admin.3dmap.fr/',
        site: 'https://3dmap.fr/',
        apiAdmin: 'https://api.3dmap.fr'
      }
    },
    pages: [
      {
        icon: 'euro_symbol',
        text: 'Ventes',
        route: '/ventes',
        type: 'page',
        active: true,
        groupes: ['commercial']
      },
      {
        icon: 'perm_contact_calendar',
        text: 'Clients',
        route: '/clients',
        type: 'page',
        active: true,
        groupes: ['commercial']
      },
      {
        icon: 'assessment',
        text: 'Statistiques',
        route: '/stats',
        type: 'page',
        active: true,
        groupes: ['commercial']
      },
      {
        icon: 'content_paste',
        text: 'Préparation',
        route: '/bonPreparation',
        type: 'page',
        active: true,
        divider: true,
        groupes: ['commercial', 'logistique']
      },
      {
        icon: 'local_shipping',
        text: 'Livraison',
        route: '/bonLivraison',
        type: 'page',
        active: true,
        divider: true,
        groupes: ['commercial', 'logistique']
      },
      {
        icon: 'shopping_cart',
        text: 'Produits',
        route: '/produits',
        type: 'page',
        active: true,
        groupes: ['webmaster']
      },
      {
        icon: 'perm_media',
        text: 'Médias',
        route: '/media',
        type: 'page',
        active: true,
        groupes: ['webmaster']
      },
      {
        icon: '3d_rotation',
        text: 'Modèles 3D',
        route: '/modeles3d',
        type: 'page',
        active: true,
        groupes: ['cartographe']
      },
      {
        icon: 'build',
        text: 'Configuration du site',
        route: '/variables',
        type: 'page',
        active: true,
        divider: true,
        groupes: ['webmaster']
      },
      {
        icon: 'description',
        text: 'Journaux comptables',
        route: '/journauxComptables',
        type: 'page',
        active: true,
        groupes: []
      },
      {
        icon: 'group',
        text: 'Utilisateurs',
        route: '/users',
        type: 'page',
        active: true,
        groupes: []
      },
      {
        icon: 'web',
        text: '3Dmap.fr',
        route: 'https://google.com/',
        type: 'application',
        active: true,
        groupes: ['all']
      },
      {
        icon: 'map',
        text: 'Map Builder',
        route: 'https://lab.3dmap.fr/geo/',
        type: 'application',
        active: true,
        groupes: ['commercial', 'cartographe']
      },
      {
        icon: 'settings',
        text: 'Admin',
        route: 'https://google.com/',
        type: 'application',
        active: true,
        groupes: ['all']
      },
      {
        icon: 'cloud',
        text: 'Drive',
        route: 'https://drive.google.com/drive/team-drives',
        type: 'application',
        active: true,
        groupes: ['all']
      }
    ]
  }
  