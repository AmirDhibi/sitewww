import axios from 'axios'
import config from '../config'

export default class {
  constructor (uri) {
    this.uri = uri
    this.connected = false
    this.login = ''
    this.nom = ''
    this.privilege = ''
    this.app = '/'
    this.sessionID = '0'

    this.notification = {
      text: '',
      type: '',
      show: false,
      delay: 3000
    }

    this.drawer = false
    this.fullscreen = false
    this.pages = config.pages

    this.api = (cmd, onUploadProgress = () => {}) => axios.create({
      method: 'post',
      baseURL: this.uri,
      headers: {
        'Session-3dmap': this.sessionID,
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })({ data: cmd, onUploadProgress })
  }



 
}
